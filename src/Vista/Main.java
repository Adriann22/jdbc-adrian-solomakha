package Vista;

import Utilidades.Conexion;

import java.sql.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Conexion conexion = new Conexion("admin", "admin", "Tienda");
        Connection conn = conexion.getConnection();

        if (conn != null) {
            System.out.println("Conexión exitosa a la base de datos.");

            int opcion;
            do {
                System.out.println("1-mostrar datos de una tabla");
                System.out.println("2-mostrar datos con una condicion");
                System.out.println("3-mostrar resultado de una funcion de agrupacion");
                System.out.println("4-insertar datos en una tabla");
                System.out.println("5-borrar datos por clave primaria");
                System.out.println("6-actualizar los parametros de una tabla");
                System.out.println("7-actualizar un parametro de uan tabla");
                System.out.println("8-salir");
                System.out.print("selecciona una opcion: ");
                opcion = scanner.nextInt();
                scanner.nextLine();

                try {
                    switch (opcion) {
                        case 1:
                            System.out.print("Ingrese el nombre de la tabla: ");
                            String tablaMostrar = scanner.nextLine();
                            mostrarTodosLosDatos(conn, tablaMostrar);
                            break;
                        case 2:
                            System.out.print("Ingrese el nombre de la tabla: ");
                            String tablaMostrar1 = scanner.nextLine();
                            System.out.print("Ingrese la condición: ");
                            String condicion = scanner.nextLine();
                            mostrarDatosConCondicion(conn, tablaMostrar1, condicion);
                            break;
                        case 3:
                            System.out.print("Ingrese el nombre de la tabla: ");
                            String tablaMostrar2 = scanner.nextLine();
                            System.out.print("Ingrese la función de agrupación (max, min, avg, sum): ");
                            String funcion = scanner.nextLine();
                            System.out.print("Ingrese el nombre de la columna: ");
                            String columna = scanner.nextLine();
                            mostrarFuncionAgregada(conn, tablaMostrar2, funcion, columna);
                            break;
                        case 4:
                            System.out.print("Ingrese el nombre de la tabla: ");
                            String tablaInsertar = scanner.nextLine();
                            System.out.print("Ingrese las columnas (separadas por coma): ");
                            String columnasInsertar = scanner.nextLine();
                            System.out.print("Ingrese los valores a insertar (separados por coma y en el mismo orden que las columnas): ");
                            String valoresInsertar = scanner.nextLine();
                            insertarDatos(conn, tablaInsertar, columnasInsertar, valoresInsertar);
                            break;
                        case 5:
                            System.out.print("Ingrese el nombre de la tabla: ");
                            String tablaBorrar = scanner.nextLine();
                            System.out.print("Ingrese el nombre de la columna de la clave primaria: ");
                            String columnaClavePrimaria = scanner.nextLine();
                            System.out.print("Ingrese el valor de la clave primaria: ");
                            String valorClavePrimaria = scanner.nextLine();
                            borrarDatosPorClavePrimaria(conn, tablaBorrar, columnaClavePrimaria, valorClavePrimaria);
                            break;

                        case 6:
                            System.out.print("Ingrese el nombre de la tabla: ");
                            String tablaActualizar = scanner.nextLine();
                            System.out.print("Ingrese los nuevos valores (columna1 = valor1, columna2 = valor2): ");
                            String nuevosValores = scanner.nextLine();
                            actualizarTodosLosParametros(conn, tablaActualizar, nuevosValores);
                            break;
                        case 7:
                            System.out.print("Ingrese el nombre de la tabla: ");
                            String tablaActualizarParametro = scanner.nextLine();
                            System.out.print("Ingrese el nombre de la columna a actualizar: ");
                            String columnaActualizar = scanner.nextLine();
                            System.out.print("Ingrese el nuevo valor: ");
                            String nuevoValor = scanner.nextLine();
                            System.out.print("Ingrese la condición (columna = valor): ");
                            String condicionActualizarUno = scanner.nextLine();
                            actualizarUnParametro(conn, tablaActualizarParametro, columnaActualizar, nuevoValor, condicionActualizarUno);
                            break;
                        case 8:
                            System.out.println("saliendo");
                            break;
                        default:
                            System.out.println("opcion invalida");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } while (opcion != 8);

            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("error al conectarse");
        }
    }
    public static void mostrarTodosLosDatos(Connection conn, String tablaMostrar) throws SQLException {
        String query = "SELECT * FROM " + tablaMostrar;
        PreparedStatement statement = conn.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();

        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        while (resultSet.next()) {
            for (int i = 1; i <= columnCount; i++) {
                String columnName = metaData.getColumnName(i);
                String value = resultSet.getString(i);

                System.out.print(columnName + ": " + value + ", ");
            }
            System.out.println();
        }

        resultSet.close();
        statement.close();
    }
    public static void mostrarDatosConCondicion(Connection conn, String tablaMostrar1, String condicion) throws SQLException {
        String query = "SELECT * FROM " + tablaMostrar1 + " WHERE " + condicion;
        PreparedStatement statement = conn.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();

        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        while (resultSet.next()) {
            for (int i = 1; i <= columnCount; i++) {
                String columnName = metaData.getColumnName(i);
                String value = resultSet.getString(i);

                System.out.print(columnName + ": " + value + ", ");
            }
            System.out.println();
        }

        resultSet.close();
        statement.close();
    }
    public static void mostrarFuncionAgregada(Connection conn, String tablaMostrar2, String funcion, String columna) throws SQLException {
        String query = "SELECT " + funcion + "(" + columna + ") AS resultado FROM " + tablaMostrar2;
        PreparedStatement statement = conn.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            double resultado = resultSet.getDouble("resultado");
            System.out.println("Resultado de " + funcion.toUpperCase() + "(" + columna + "): " + resultado);
        }

        resultSet.close();
        statement.close();
    }
    public static void insertarDatos(Connection conn, String tabla, String columnas, String valores) throws SQLException {
        String query = "INSERT INTO " + tabla + " (" + columnas + ") VALUES (" + valores + ")";
        PreparedStatement statement = conn.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }
    public static void borrarDatosPorClavePrimaria(Connection conn, String tabla, String columnaClavePrimaria, String valorClavePrimaria) throws SQLException {
        String query = "DELETE FROM " + tabla + " WHERE " + columnaClavePrimaria;
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, valorClavePrimaria);
        statement.executeUpdate();
        statement.close();
    }
    public static void actualizarTodosLosParametros(Connection conn, String tabla, String nuevosValores) throws SQLException {
        String query = "UPDATE " + tabla + " SET " + nuevosValores;
        PreparedStatement statement = conn.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }
    public static void actualizarUnParametro(Connection conn, String tabla, String columna, String nuevoValor, String condicion) throws SQLException {
        String query = "UPDATE " + tabla + " SET " + columna + " WHERE " + condicion;
        PreparedStatement statement = conn.prepareStatement(query);
        statement.setString(1, nuevoValor);
        statement.executeUpdate();
        statement.close();
    }
}
