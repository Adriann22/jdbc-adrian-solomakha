DROP DATABASE Tienda;
CREATE DATABASE IF NOT EXISTS Tienda;
USE Tienda;

CREATE TABLE IF NOT EXISTS clientes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    email VARCHAR(100),
    telefono VARCHAR(20),
    direccion VARCHAR(100),
    ciudad VARCHAR(50),
    pais VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS productos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    descripcion TEXT,
    precio DECIMAL(10, 2) NOT NULL,
    cantidad_stock INT NOT NULL,
    categoria VARCHAR(50),
    proveedor VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS pedidos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    cliente_id INT NOT NULL,
    producto_id INT NOT NULL,
    cantidad INT NOT NULL,
    fecha_pedido DATE NOT NULL,
    estado_pedido VARCHAR(20),
    FOREIGN KEY (producto_id) REFERENCES productos(id)
);

-- Insertar datos en la tabla clientes
INSERT INTO clientes (nombre, apellido, email, telefono, direccion, ciudad, pais)
VALUES
    ('Juan', 'Pérez', 'juan@example.com', '123456789', 'Calle Principal 123', 'Ciudad de México', 'México'),
    ('María', 'García', 'maria@example.com', '987654321', 'Avenida Central 456', 'Madrid', 'España'),
    ('John', 'Smith', 'john@example.com', '5551234', '123 Main St', 'New York', 'USA');

-- Insertar datos en la tabla productos
INSERT INTO productos (nombre, descripcion, precio, cantidad_stock, categoria, proveedor)
VALUES
    ('Camisa Polo', 'Camisa de algodón de manga corta', 29.99, 50, 'Ropa', 'Proveedor A'),
    ('Zapatos Deportivos', 'Zapatos para correr de alta calidad', 59.99, 30, 'Calzado', 'Proveedor B'),
    ('Teléfono Móvil', 'Smartphone con pantalla táctil', 399.99, 100, 'Electrónica', 'Proveedor C');

-- Insertar datos en la tabla pedidos
INSERT INTO pedidos (cliente_id, producto_id, cantidad, fecha_pedido, estado_pedido)
VALUES
    (1, 1, 2, '2024-03-15', 'En proceso'),
    (2, 2, 1, '2024-03-16', 'Entregado'),
    (3, 3, 3, '2024-03-17', 'Enviado');

